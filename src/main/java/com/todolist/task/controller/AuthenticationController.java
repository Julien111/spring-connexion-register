package com.todolist.task.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.todolist.task.dto.UserDto;
import com.todolist.task.entity.User;
import com.todolist.task.service.SecurityService;
import com.todolist.task.service.UserService;

@Controller
public class AuthenticationController {
	
	@Autowired
	private UserService userService;

	@Autowired
	private SecurityService securityService;
	
	@GetMapping("/")
	public String homePage() {
		return "index";
	}

	@GetMapping("/login")
	public String loginForm(Model model, String error) {
		// if the user is connect
		if (securityService.isAuthenticated()) {
            return "redirect:/users";
        }
		
		if(error != null) {
			model.addAttribute("error", "Votre mot de passe ou votre e-mail est invalide.");
		}
		
		return "login";
	}

	@GetMapping("/register")
	public String showRegisterForm(Model model) {
		// if the user is connect
		if (securityService.isAuthenticated()) {
			return "redirect:/users";
		}
		// create model of object

		UserDto userDto = new UserDto();
		model.addAttribute("user", userDto);
		return "register";
	}

	@PostMapping("/register/save")
	public String registration(@Valid @ModelAttribute("user") UserDto userDto, BindingResult result, Model model) {

		User existingUser = userService.findUserByEmail(userDto.getEmail());

		if (existingUser != null && existingUser.getEmail() != null && !existingUser.getEmail().isEmpty()) {
			result.rejectValue("email", null, "Cet E-mail est déjà utilisé pour un autre compte");
		}

		if (result.hasErrors()) {
			model.addAttribute("user", userDto);
			return "/register";
		}

		userService.saveUser(userDto);
		return "redirect:/login?success";
	}

	/**
	 * Have a list of users
	 * 
	 * @param model
	 * @return string
	 */
	@GetMapping("/users")
	public String users(Model model) {
		List<UserDto> users = userService.findAllUsers();
		model.addAttribute("users", users);
		return "users";
	}

}
