package com.todolist.task.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.todolist.task.entity.User;

public interface UserDao extends JpaRepository<User, Long> {
	
	User findByEmail(String email);
}
