package com.todolist.task.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

	private Long id;
	
	@NotEmpty(message="Le champ ne doit pas être vide")
	private String firstName;
	
	@NotEmpty(message="Le champ ne doit pas être vide")
	private String lastName;
	
	@Email(message="L'email n'est pas valide")
	private String email;

	@NotEmpty(message = "Le mot de passe ne doit pas êtres vide")
	private String password;

}
