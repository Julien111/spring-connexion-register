package com.todolist.task.service;

import java.util.List;

import com.todolist.task.dto.UserDto;
import com.todolist.task.entity.User;

public interface UserService {
	
	void saveUser(UserDto userDto);
	
	User findUserByEmail(String email);
	
	List<UserDto> findAllUsers();

}
