package com.todolist.task.service;

public interface SecurityService {
	boolean isAuthenticated();
    void authenticationLogin(String email, String password);
}
