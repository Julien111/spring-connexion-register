package com.todolist.task.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.todolist.task.dao.RoleDao;
import com.todolist.task.dao.UserDao;
import com.todolist.task.dto.UserDto;
import com.todolist.task.entity.Role;
import com.todolist.task.entity.User;
import com.todolist.task.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	private UserDao userDao;
	
	private RoleDao roleDao;
	
	private PasswordEncoder passwordEncoder;	
	
	/**
	 * Constructor
	 * @param userDao
	 * @param roleDao
	 * @param passwordEncoder
	 */
	public UserServiceImpl(UserDao userDao, RoleDao roleDao, PasswordEncoder passwordEncoder) {
		super();
		this.userDao = userDao;
		this.roleDao = roleDao;
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public void saveUser(UserDto userDto) {
		User user = new User();
		
		user.setFirstName(userDto.getFirstName());
		user.setLastName(userDto.getLastName());
		user.setEmail(userDto.getEmail());
		//encrypt password
		user.setPassword(passwordEncoder.encode(userDto.getPassword()));
		
		//role
		Role role = roleDao.findByName("ROLE_USER");
        if(role == null){
            role = createRole();
        }
        user.setRoles(Arrays.asList(role));
		userDao.save(user);
	}

	@Override
	public User findUserByEmail(String email) {
		return userDao.findByEmail(email);
	}

	@Override
	public List<UserDto> findAllUsers() {
		List<User> users = userDao.findAll();
		return users.stream().map(this::mapToUserAtUserDto).collect(Collectors.toList());
	}
	
	/**
	 * Replace user by user dto.
	 * @param user
	 * @return
	 */
	private UserDto mapToUserAtUserDto(User user) {
		UserDto userDto = new UserDto();
		userDto.setId(user.getId());
		userDto.setEmail(user.getEmail());
		userDto.setFirstName(user.getFirstName());
		userDto.setLastName(user.getLastName());
		return userDto;
	}
	
	/**
	 *  Create the role user
	 * @return
	 */
	private Role createRole() {
		Role role = new Role();
		role.setName("ROLE_USER");
		return roleDao.save(role);
	}
	

}
